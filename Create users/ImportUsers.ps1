Import-Module ActiveDirectory

./SiteConfig.ps1
$Users = Import-Csv -Path "C:\Data\SjWaroona\CSV Class List 2017.csv"
 
foreach ($User in $Users)            
{            
    $Displayname = $User.'Firstname' + " " + $User.'Lastname'            
    $UserFirstname = $User.'Firstname'            
    $UserLastname = $User.'Lastname'            
    $OU = 'OU=Year4,OU=Students,OU=SJPS USERS,DC=SJPSDOMAIN,DC=internal'         
    $SAM = $User.'Lastname' + "." + $User.'Firstname'          
    $UPN = $SAM + "@stjwaroona.wa.edu.au"            
    $Description = $User.'Description'            
    $Password = $User.'Password'            
    #New-ADUser -Name "$Displayname" -DisplayName "$Displayname" -SamAccountName $SAM -UserPrincipalName $UPN -GivenName "$UserFirstname" -Surname "$UserLastname" -Description "$Description" -AccountPassword (ConvertTo-SecureString $Password -AsPlainText -Force) -Enabled $true -Path "$OU" -ChangePasswordAtLogon $false –PasswordNeverExpires $true  
    Write-Host "-Name $Displayname `n -DisplayName $Displayname `n -SamAccountName $SAM `n -EmailAddress $UPN `n -GivenName $UserFirstname `n -Surname $UserLastname `n -Description $Description `n -AccountPassword (ConvertTo-SecureString $Password -AsPlainText -Force) `n -Enabled $true `n -Path $OU `n -ChangePasswordAtLogon $false `n –PasswordNeverExpires $true  "
    Write-Host "-----------------------"
}