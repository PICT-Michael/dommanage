﻿# Organisation Units for active directory
$SiteStaffOU = 
$SiteAdminOU = 
# Kindy
$SiteStudentOU0 =
# Year 1
$SiteStudentOU1 =
# Year 2
$SiteStudentOU2 =
# Year 3
$SiteStudentOU3 =
# Year 4
$SiteStudentOU4 = 'OU=Year4,OU=Students,OU=SJPS USERS,DC=SJPSDOMAIN,DC=internal'
# Year 5
$SiteStudentOU5 =
# Year 6
$SiteStudentOU6 =
# Year 7
$SiteStudentOU7 =
# Year 8
$SiteStudentOU8 =

# Email Domain do not include @
$SiteEmailtld = 'sjwaroona.wa.edu.au'